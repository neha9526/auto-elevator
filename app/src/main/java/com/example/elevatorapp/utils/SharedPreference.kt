package com.example.elevatorapp.utils

import android.content.Context
import android.content.SharedPreferences

object SharedPreference {

    private const val userNameSession ="USER_NAME_SESSION"
    private const val conUserName ="USER_NAME"


    //get set username
    fun setUserName(context: Context,userName:String){
        val editor:SharedPreferences.Editor= context.getSharedPreferences(userNameSession,Context.MODE_PRIVATE).edit()
        editor.putString(conUserName,userName)
        editor.apply()
    }

    fun getUserName(context: Context):String?{
        val sharedPref= context.getSharedPreferences(userNameSession,Context.MODE_PRIVATE)
        return sharedPref.getString(conUserName,"")
    }







}
