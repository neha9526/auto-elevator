package com.example.elevatorapp.utils

class Constants {

    object REGEX {
        val REGEX_NAME_FILEDS = "^[\\.,';:\"\\-\\(\\)\\[\\]{}<>_a-zA-Z ]+$"
        val REGEX_ALPHABETS_FILEDS = "[a-zA-Z ]+"
        val REGEX_ALPHANUMERIC_FILEDS = "[a-zA-Z0-9 ]+"
        val REGEX_ADDRESS = "[a-zA-Z0-9-/., ]+"
        val REGEX_PAN_NO = "[A-Z]{3}[A,B,C,F,H,J,L,P,R,T]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}"
        val REGEX_EMAIL = "[a-zA-Z0-9._-]+@[a-z]+\\\\.+[a-z]+"
        val REGEX_MOB_NO = "[0-9]+"
    }
}