package com.example.elevatorapp.utils

import android.app.Activity
import android.content.Context
import android.support.design.widget.TextInputLayout
import android.text.Editable
import android.text.InputFilter
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import java.util.regex.Pattern

object MethodUtils {
    lateinit var fieldPattern:Pattern

    fun setEdittextValidation(edittextFilterModelList:ArrayList<EdittextFilterModel>)
    {
        try {
            for(editTextFilModel in edittextFilterModelList)
            {
              editTextFilModel.editText.addTextChangedListener(object :TextWatcher{
                  override fun afterTextChanged(p0: Editable?) {}

                  override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                  override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                      val newText = p0.toString()
                      if(newText.isNotEmpty()&& p1 < newText.length)
                      {
                          fieldPattern = editTextFilModel.pattern ?: Pattern.compile(editTextFilModel.regex)

                          if (!fieldPattern.matcher(p0).matches())
                          {
                              val newString = newText.substring(0, p1) + newText.substring(p1 + 1)
                              editTextFilModel.editText.setText(newString)
                              if (newText.length > p1) {
                                  editTextFilModel.editText.setSelection(p1)
                              }
                          }
                      }
                  }

              })
                val inputFilters: Array<InputFilter?>
                if (editTextFilModel.allCaps) {
                    inputFilters = arrayOfNulls(2)
                    inputFilters[1] = InputFilter.AllCaps()
                } else {
                    inputFilters = arrayOfNulls(1)
                }
                inputFilters[0] = InputFilter.LengthFilter(editTextFilModel.length)
                editTextFilModel.editText.filters = inputFilters

            }
        }
        catch (e:Exception)
        {
            e.printStackTrace()
        }
    }
    fun showKeyboard(activity: Activity, view: View) {
        try {
            view.requestFocus()
            val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInputFromWindow(view.applicationWindowToken, InputMethodManager.SHOW_FORCED, 3)
        } catch (e: Exception) {
          e.printStackTrace()
        }

    }





}