package com.example.elevatorapp.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by admin on 31-01-2018.
 */
object DateHelper {
    private val MONTH = SimpleDateFormat("MM", Locale.ENGLISH)
    private val STRING_MONTH = SimpleDateFormat("MMM", Locale.ENGLISH)
    private val DAY = SimpleDateFormat("dd", Locale.ENGLISH)
    private val YEAR = SimpleDateFormat("yyyy", Locale.ENGLISH)
    private val HOURS = SimpleDateFormat("HH", Locale.ENGLISH)
    private val MINUTE = SimpleDateFormat("mm", Locale.ENGLISH)
    private val SECOND = SimpleDateFormat("ss", Locale.ENGLISH)
    private val DATE_ONLY = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    private val TIME_ONLY = SimpleDateFormat("hh:mm:ss a", Locale.ENGLISH)
    private val DATA_DOC_TIME = SimpleDateFormat("dd/MM/yy hh:mm a", Locale.ENGLISH)
    val DATE_TIME=SimpleDateFormat("yyyy-MMMM-dd HH:mm:ss")
    private val YEAR_MONTH_DATE_TIME=SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private val WEEK = SimpleDateFormat("EEE", Locale.ENGLISH)
    private val DATE_FILTER = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
    private val time_sdf= SimpleDateFormat("hh:mm a")
    private val YEAR_MONTH_DATE = SimpleDateFormat("yyyy-MMMM-dd", Locale.ENGLISH)
    val SAVE_FILE_FORMAT_FULL_DATE = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)



    fun lastDateFormat(dateString:String):String
    {
        var date: Date? = null
        try {
            date = YEAR_MONTH_DATE_TIME.parse(dateString)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return DATA_DOC_TIME.format(date)
    }

    fun getYear(millis: Long): String {
        val currentDate = Date(millis)
        return YEAR.format(currentDate)
    }

    fun getMonth(millis: Long): String {
        val currentDate = Date(millis)
        return MONTH.format(currentDate)
    }


    fun getDay(millis: Long): String {
        val currentDate = Date(millis)
        return DAY.format(currentDate)
    }

    val todayCalender = Calendar.getInstance()!!

    fun getYearMonthDateFormat(month: Int, year: Int, dateOfMonth: Int): String {
        val cal = Calendar.getInstance()
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.DAY_OF_MONTH, dateOfMonth)
        return DATE_ONLY.format(cal.time)
    }
    fun getFilterFormat2(times:String): String {
        var date: Date? = null
        try {
            date = DATE_ONLY.parse(times)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return DATE_FILTER.format(date)
    }
    fun getDateFilterFormat(month: Int, year: Int, dateOfMonth: Int): String {
        val cal = Calendar.getInstance()
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.DAY_OF_MONTH, dateOfMonth)
        return DATE_FILTER.format(cal.time)
    }
 fun getDate(millis: Long):String
 {
     val currentTime =Date(millis)
     return  DATE_ONLY.format(currentTime)
 }
    fun getFormattedDate(times: String): String {
        var date: Date? = null
        try {
             date = DATE_TIME.parse(times)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return DATE_ONLY.format(date)
    }
    fun getFormattedMonth(times: String):String
    {
        var date: Date? = null
        try {
            date = YEAR_MONTH_DATE_TIME.parse(times)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return STRING_MONTH.format(date)
    }

    fun dateFilterFormat(times: String):String
    {
        var date: Date? = null
        try {
            date = YEAR_MONTH_DATE_TIME.parse(times)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return DATE_FILTER.format(date)
    }
  fun getFormattedYear(times: String):String
  {
      var date: Date? = null
      try {
          date = YEAR_MONTH_DATE_TIME.parse(times)
      }
      catch (e: ParseException) {
          e.printStackTrace()
      }
      return YEAR.format(date)
  }


    fun getFormattedDay(times: String):String
    {
        var date: Date? = null
        try {
            date = YEAR_MONTH_DATE_TIME.parse(times)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return DAY.format(date)
    }
    fun getDateTime(time:Long):String
    {
        val currentTime =Date(time)
        return DATA_DOC_TIME.format(currentTime)
    }

    fun getDateTimeNew(time:Long):String
    {
        val currentTime =Date(time)
        return YEAR_MONTH_DATE_TIME.format(currentTime)
    }


    fun getFormattedTime(times: String):String
    {
        var date: Date? = null
        try {
            date = YEAR_MONTH_DATE_TIME.parse(times)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return TIME_ONLY.format(date)
    }

    fun getDatabaseFormat(time:String):String
    {
        var date: Date? = null
        try {
            date = YEAR_MONTH_DATE_TIME.parse(time)
        }
        catch (e: ParseException) {
            e.printStackTrace()
        }
        return  DATE_ONLY.format(date)
    }

    fun getDateCalender(inputDate :String) : Calendar {
        val cal = Calendar.getInstance()
        try {
            cal.time = DATE_ONLY.parse(inputDate)
        } catch (e: Exception) {
            println("Inappropriate string")
        }
        return cal
    }
}