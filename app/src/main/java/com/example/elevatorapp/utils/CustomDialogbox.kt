package com.example.elevatorapp.utils

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatEditText
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.example.elevatorapp.R
import com.example.elevatorapp.dashboard.HomeScreen
import com.example.elevatorapp.login.LoginActivity
import kotlinx.android.synthetic.main.layout_otp_fragment.view.*

class CustomDialogbox : DialogFragment() {
    lateinit var otpNo1: AppCompatEditText
    lateinit var otpNo2: AppCompatEditText
    lateinit var otpNo3: AppCompatEditText
    lateinit var otpNo4: AppCompatEditText
    lateinit var btnSubmitOTP: AppCompatButton
    private var mActivity:LoginActivity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = activity as LoginActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialogView = inflater.inflate(R.layout.layout_otp_fragment, container, false)

        otpNo1 = dialogView.etOtpNo1
        otpNo2 = dialogView.etOtpNo2
        otpNo3 = dialogView.etOtpNo3
        otpNo4 = dialogView.etOtpNo4
        btnSubmitOTP = dialogView.btnSubmitOTP


        otpNo1.addTextChangedListener(GenericTextWatcher(R.id.etOtpNo1))
        otpNo2.addTextChangedListener(GenericTextWatcher(R.id.etOtpNo2))
        otpNo3.addTextChangedListener(GenericTextWatcher(R.id.etOtpNo3))
        otpNo4.addTextChangedListener(GenericTextWatcher(R.id.etOtpNo4))

        otpNo1.requestFocus()


        btnSubmitOTP.setOnClickListener()
        {
            mActivity!!.progressBar!!.visibility = View.VISIBLE
            startActivity(Intent(activity, HomeScreen::class.java))
            dismiss()
        }

        return dialogView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }


    //Generic Textwatcher
    inner class GenericTextWatcher(val id: Int) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            try {
                val textValue = s.toString()
                when (id) {
                    R.id.etOtpNo1 -> {
                        if (textValue.length == 1)
                            otpNo2.requestFocus()
                        else {
                            otpNo1.requestFocus()

                        }
                    }

                    R.id.etOtpNo2 -> {
                        if (textValue.length == 1)
                            otpNo3.requestFocus()
                        else if (textValue.isEmpty())
                            otpNo1.requestFocus()
                    }
                    R.id.etOtpNo3 -> {
                        if (textValue.length == 1)
                            otpNo4.requestFocus()
                        else if (textValue.isEmpty())
                            otpNo2.requestFocus()
                    }
                    R.id.etOtpNo4 -> {
                        if (textValue.isEmpty())
                            otpNo3.requestFocus()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }



}
