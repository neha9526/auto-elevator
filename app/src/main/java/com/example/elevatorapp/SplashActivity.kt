package com.example.elevatorapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.example.elevatorapp.login.LoginActivity
import com.example.elevatorapp.R
import kotlinx.android.synthetic.main.activity_main.*


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        progressbarLayout.visibility = View.VISIBLE
        //starting bluetooth connection
        val handler = Handler()
        handler.postDelayed(Runnable {
            // Do something after 5s = 5000ms
            progressbarLayout.visibility = View.GONE
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, 3000)
        
        
    }
}
