package com.example.elevatorapp.dashboard

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import com.example.elevatorapp.R
import com.example.elevatorapp.dashboard.ViewPagerAdpter
import com.example.elevatorapp.utils.EventHandler
import kotlinx.android.synthetic.main.activity_home_screen.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.greenrobot.eventbus.EventBus

class HomeScreen : AppCompatActivity(), TabLayout.OnTabSelectedListener {


    override fun onTabReselected(p0: TabLayout.Tab?) {}

    override fun onTabUnselected(p0: TabLayout.Tab?) {}

    override fun onTabSelected(p0: TabLayout.Tab?) {
        vpDashboard.currentItem = p0!!.position;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)
        try {

            setSupportActionBar(customToolbar)
            // activityName.text = "Elevator App"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            //setting viewpager
            val fm = supportFragmentManager
            vpDashboard.adapter = ViewPagerAdpter(fm, tabLayout.tabCount)
            tabLayout.addOnTabSelectedListener(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }


    override fun onBackPressed() {
        super.onBackPressed()
        EventBus.getDefault().post(EventHandler(null))
    }


}
