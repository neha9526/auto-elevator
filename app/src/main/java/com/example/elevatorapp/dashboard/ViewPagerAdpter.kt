package com.example.elevatorapp.dashboard

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class ViewPagerAdpter(val fm:FragmentManager, val tabCount:Int):FragmentStatePagerAdapter(fm) {

/*    val bundle: Bundle = Bundle()
    bundle.putString("NAME",userName)
    fm.putFragment(bundle,"BUNDLE",DashboardFragment())*/

    override fun getItem(p0: Int): Fragment {
        return when(p0){
            0->{

                DashboardFragment()
            }
            1->{
                OperateElevFragment()
            }
            else->{
                DashboardFragment()
            }
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}