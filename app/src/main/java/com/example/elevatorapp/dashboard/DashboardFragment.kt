package com.example.elevatorapp.dashboard

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.elevatorapp.R
import com.example.elevatorapp.configElev.ConfigElevActivity
import com.example.elevatorapp.adapter.HomeScreenAdapter
import com.example.elevatorapp.health_check.HealthStatusActivity
import com.example.elevatorapp.maintenance_mode.MaintenanceActivity
import com.example.elevatorapp.service_report.ServiceReportActivity
import com.example.elevatorapp.utils.SharedPreference
import kotlinx.android.synthetic.main.fragment_dashboard_layout.*


class DashboardFragment(): Fragment(), HomeScreenAdapter.OnItemClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_dashboard_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etLoggedInUser.text = SharedPreference.getUserName(activity!!.applicationContext)
        rvHomeMenu.layoutManager = GridLayoutManager(activity, 2)
        val menuList: ArrayList<HomeMenuPojo> = ArrayList()
        menuList.add(HomeMenuPojo(getString(R.string.maintenance_menu), R.drawable.ic_maintenance))
        menuList.add(HomeMenuPojo(getString(R.string.health_check_menu), R.drawable.ic_health_check))
        menuList.add(HomeMenuPojo(getString(R.string.configure_elv_menu), R.drawable.ic_config_elev_2))
        menuList.add(HomeMenuPojo(getString(R.string.service_report_menu), R.drawable.ic_report_generation))

        rvHomeMenu.adapter = HomeScreenAdapter(this, menuList)
    }

    //to implement onclick listener
    override fun onMenuSelected(menuName: String) {
        when (menuName) {
            getString(R.string.maintenance_menu) -> {
                startActivity(Intent(activity, MaintenanceActivity::class.java))
            }

            getString(R.string.health_check_menu) -> {
                startActivity(Intent(activity,HealthStatusActivity::class.java))

            }
            getString(R.string.configure_elv_menu) -> {
                startActivity(Intent(activity, ConfigElevActivity::class.java))
            }
            getString(R.string.service_report_menu) -> {
                startActivity(Intent(activity, ServiceReportActivity::class.java))

            }
        }
    }

    class HomeMenuPojo(val menuName: String, val menuIcon: Int) {}
}
