package com.example.elevatorapp.login

import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.view.View
import android.widget.ProgressBar
import com.example.elevatorapp.R
import com.example.elevatorapp.utils.CustomDialogbox
import com.example.elevatorapp.utils.*
import com.example.elevatorapp.utils.Constants.REGEX.REGEX_ALPHANUMERIC_FILEDS
import kotlinx.android.synthetic.main.activity_login.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LoginActivity : AppCompatActivity() {

    var progressBar:ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //view initialization
        progressBar = findViewById(R.id.loginProgressbar)
        //register event bus
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this)
        }

        //validations
        val listEditextFilters: ArrayList<EdittextFilterModel> = ArrayList()
        listEditextFilters.add(EdittextFilterModel(etUserName, REGEX_ALPHANUMERIC_FILEDS, 20,true,null))
        //listEditextFilters.add(EdittextFilterModel(etUserPassword, PASSWORDREGEX,12))
        MethodUtils.setEdittextValidation(listEditextFilters)

        val inputFiletr: Array<InputFilter> = arrayOf(InputFilter.LengthFilter(10))
        etMobileNo.filters = inputFiletr

        btnLoginBtn.setOnClickListener()
        {

            try {
                when {
                    etUserName.text.isNullOrEmpty() -> {
                        // tiletUserName.e
                        return@setOnClickListener
                    }
                    etMobileNo.text.isNullOrEmpty() -> {
                        tileMobNo.error = "Enter Mobile No";
                        return@setOnClickListener
                    }
                    else -> {
                        SharedPreference.setUserName(this,etUserName.text.toString())
                        showDialog()
                        /* val handler = Handler()
                         handler.postDelayed(Runnable {
                           //  progressbarLayout.visibility = View.GONE
                             finish()
                         }, 3000)*/
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showDialog() {
        val fragmentManager = supportFragmentManager
        val newFragment = CustomDialogbox()
        // The device is smaller, so show the fragment fullscreen
        val transaction = fragmentManager.beginTransaction()
        // For a little polish, specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit()
    }


    override fun onDestroy() {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().unregister(this)
        }
    }

    //listen event
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLogout(even:EventHandler){
        etUserName.setText("")
        etMobileNo.setText("")
    }


    override fun onStop() {
        super.onStop()
        if(progressBar!=null && progressBar!!.visibility == View.VISIBLE)
        {
            progressBar!!.visibility = View.GONE
        }
    }



}
