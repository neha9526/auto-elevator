package com.example.elevatorapp.adapter

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.elevatorapp.dashboard.DashboardFragment
import com.example.elevatorapp.R
import kotlinx.android.synthetic.main.adapter_dashboard_layout.view.*

class HomeScreenAdapter(private val fragment: DashboardFragment,
                        private val menuList:ArrayList<DashboardFragment.HomeMenuPojo>):
    RecyclerView.Adapter<HomeScreenAdapter.CustomViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CustomViewHolder {
      val view:View = LayoutInflater.from(fragment.activity).inflate(R.layout.adapter_dashboard_layout,p0,false)
        return  CustomViewHolder(view)
    }

    override fun getItemCount(): Int {
        return menuList.size
    }

    override fun onBindViewHolder(p0: CustomViewHolder, p1: Int) {
        p0.menuIcon.setImageResource(menuList[p1].menuIcon)
        p0.menuName.text = menuList[p1].menuName

        //setting on click listner
        p0.menuLayout.setOnClickListener()
        {
            fragment.onMenuSelected(menuList[p1].menuName)
        }
    }


    class CustomViewHolder(item: View):RecyclerView.ViewHolder(item)
    {
        val menuIcon:AppCompatImageView = item.ivMenuIcon
        val menuName: AppCompatTextView  = item.tvMenuName
        val menuLayout:LinearLayout = item.menuLayout
    }


    interface OnItemClickListener
    {
        fun onMenuSelected(menuName:String)
    }
}


