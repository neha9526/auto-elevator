package com.example.elevatorapp.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.elevatorapp.R
import com.example.elevatorapp.health_check.HealthStatusPojo
import kotlinx.android.synthetic.main.adapter_main_rv_layout.view.*

class HealthCheckMainAdapter(private val activity: Activity, private val mainList:ArrayList<HealthStatusPojo>,
                            val onItemClickListener: OnItemClickListener):
    RecyclerView.Adapter<HealthCheckMainAdapter.MainViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MainViewHolder {
        val view:View = LayoutInflater.from(activity).inflate(R.layout.adapter_main_rv_layout,p0,false)
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
       return mainList.size
    }

    override fun onBindViewHolder(p0: MainViewHolder, p1: Int) {
        p0.tvElevBoard.text = mainList[p1].boardName
        if(mainList[p1].healthStatus)
        {
            p0.ivHealthStatus.setImageResource(R.drawable.ic_right_tick)
        }
        else{
            p0.ivHealthStatus.setImageResource(R.drawable.ic_wrong_mark_icon)
        }

        p0.listItemLayout.setOnClickListener()
        {
            onItemClickListener.onItemClick()
        }


    }


    class MainViewHolder(item: View):RecyclerView.ViewHolder(item)
    {
        val tvElevBoard:AppCompatTextView = item.tvElevBoard
        val ivHealthStatus = item.ivHealthStatus
        val listItemLayout:CardView = item.listItemLayout
        val rvSubSection = item.rvSubSection

    }


    interface OnItemClickListener{
        fun onItemClick()
    }
}