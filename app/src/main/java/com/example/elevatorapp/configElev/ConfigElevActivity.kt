package com.example.elevatorapp.configElev

import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.elevatorapp.R
import kotlinx.android.synthetic.main.activity_config_elev.*
import kotlinx.android.synthetic.main.layout_numberpicker.view.*
import kotlinx.android.synthetic.main.textviewforspinner.*
import kotlinx.android.synthetic.main.textviewforspinner.view.*
import kotlinx.android.synthetic.main.textviewforspinner.view.spinnerTextview
import kotlinx.android.synthetic.main.toolbar_layout.*
import java.lang.Exception

class ConfigElevActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_config_elev)
        try {
            setSupportActionBar(customToolbar)
            activityName.text = getString(R.string.configure_elv_menu)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)


            //set spinners
            //baudrate spinner
           /* val baudRateList = arrayOf("Select Baud Rate", "2400 bps", "9600 bps",
                "4800 bps", "19200 bps", "38400 bps", "57600 bps", "115200 bps")*/
            initializeSpinners(R.array.baudRate,spBaudRate)
            initializeSpinners(R.array.baudRate,spIbBaudRate)

           // val stopBitList = arrayOf("Select Stop Bit","1","2")
            initializeSpinners(R.array.startStopBit,spStopBit)
            initializeSpinners(R.array.startStopBit,spIbStopBit)

           // val parityBitList = arrayOf("Select Parity Bit","Enable","Disable")
            initializeSpinners(R.array.parityBit,spParityBit)
            initializeSpinners(R.array.parityBit,spIBParityBit)

            val commModeList = arrayOf("Select Comm Mode","ASCIII","RTU")
             initializeSpinners(R.array.commMode,spCommMode)
             initializeSpinners(R.array.commMode,spIBCommMode)

            //radio button listener
            rgDoorWorking.setOnCheckedChangeListener { p0, p1 ->
                val rbCurrent = findViewById<RadioButton>(p1)
                rbCurrent.typeface = Typeface.DEFAULT_BOLD
            }

            rgDuplexMode.setOnCheckedChangeListener{ p0, p1 ->
                val rbCurrent = findViewById<RadioButton>(p1)
                rbCurrent.typeface = Typeface.DEFAULT_BOLD
            }

            //listeners for floor no settings
            tvSelectMaxPosFloor.setOnClickListener()
            {
                showNumberPickerDailog("Set Max +Ve Floors", tvSelectMaxPosFloor, 64, "")
            }

            tvSelectMaxNegFloor.setOnClickListener()
            {
                showNumberPickerDailog("Set Max -Ve Floors", tvSelectMaxNegFloor, 8, "")
            }

            //set speed setting listeners
            tvSelectHighSpeed.setOnClickListener()
            {
                showNumberPickerDailog("Set High Speed", tvSelectHighSpeed, 10, "m/Sec")
            }
            tvSelectLowSpeed.setOnClickListener()
            {
                showNumberPickerDailog("Set Low Speed", tvSelectLowSpeed, 10, "m/Sec")
            }

            //set listeners for timeout settings
            tvSelectDoorOpenTimeout.setOnClickListener()
            {
                showNumberPickerDailog("Set Door Open Timeout", tvSelectDoorOpenTimeout, 20, "Sec")
            }
            tvSelectDoorCloseTimeout.setOnClickListener()
            {
                showNumberPickerDailog("Set Door Close Timeout", tvSelectDoorCloseTimeout, 20, "Sec")
            }
            tvSelectCabinIdealPeriod.setOnClickListener()
            {
                showNumberPickerDailog("Set Cabin Idel Period", tvSelectCabinIdealPeriod, 20, "Sec")
            }


            btnDonSpeed.setOnClickListener()
            {

            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }



    private fun showNumberPickerDailog(title: String, textView: AppCompatTextView, maxLimit: Int, unit: String) {

        val inflater = LayoutInflater.from(this)
        val dialogView = inflater.inflate(R.layout.layout_numberpicker, null)
        val dialogBuilder = AlertDialog.Builder(this)

        //initializing views
        val titleMsg = dialogView.tvDialogTitle as AppCompatTextView
        val numberPicker = dialogView.npTimeout as NumberPicker
        val btnOk = dialogView.btnOK as AppCompatButton
        val btnCancel = dialogView.btnCancel as AppCompatButton
        var selectedValue = 0

        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog? = dialogBuilder.create()
        if (!alertDialog!!.isShowing)
            alertDialog.show()

        //setting fields
        titleMsg.text = title
        numberPicker.maxValue = maxLimit
        numberPicker.minValue = 1
        numberPicker.wrapSelectorWheel = true

        numberPicker.setOnValueChangedListener { p0, oldValue, newvalue -> selectedValue = newvalue }

        btnOk.setOnClickListener()
        {
            textView.text = selectedValue.toString() + " " + unit
            alertDialog.dismiss()

        }

        btnCancel.setOnClickListener()
        {
            alertDialog.dismiss()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if(p2==0)
        {
            p0!!.spinnerTextview.setTextColor(ContextCompat.getColor(this, R.color.silver_grey))
        }
        else{
            p0!!.spinnerTextview.setTextColor(ContextCompat.getColor(this, R.color.black))
            p0.spinnerTextview.setTypeface(spinnerTextview.typeface, Typeface.BOLD)

        }
    }


    private fun initializeSpinners(arrayList:Int, spinner:Spinner) {

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this, arrayList, R.layout.textviewforspinner).also { spinnerAdapter ->
            // Specify the layout to use when the list of choices appears
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
           spinner.adapter = spinnerAdapter
        }
        spinner.onItemSelectedListener = this
    }

}
