package com.example.elevatorapp.service_report

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.graphics.pdf.PdfDocument.PageInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageView
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import com.example.elevatorapp.BuildConfig
import com.example.elevatorapp.R
import com.example.elevatorapp.utils.*
import com.example.elevatorapp.service_report.Signature
import kotlinx.android.synthetic.main.layout_pdf_format.*
import kotlinx.android.synthetic.main.toolbar_layout.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class ServiceReportActivity : AppCompatActivity() {
    private lateinit var layoutBitmap: Bitmap
    private lateinit var pdfFilePath: File
    private lateinit var firstErrorView: View
    private val listPermissionsNeeded = ArrayList<String>()
    private var validCount = 0
    private val CAMERA_REQUEST: Int = 100
    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 1
    private lateinit var mSignature: Signature
    private lateinit var dialog: Dialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            setContentView(R.layout.layout_pdf_format)
            //setting toolbar
            setSupportActionBar(customToolbar)
            activityName.text = getString(R.string.service_report_menu)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)


            //creating permission list
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)

            //initialize views
            //set spinners
            val serviceTypesList = arrayOf("Select Service Type", "Installation", "Re-installation", "AMC")
            val serviceTypeAdapter = ArrayAdapter(this, R.layout.textviewforspinner, serviceTypesList)
            spServiceType.adapter = serviceTypeAdapter


            // Dialog Function
            dialog = Dialog(this)
            // Removing the features of Normal Dialogs
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.layout_signature_dialog)
            dialog.setCancelable(true)



            //listeners
            btnAddImage.setOnClickListener()
            {
                //open camera
                if (hasPermissions(listPermissionsNeeded)) {
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    intent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                    startActivityForResult(intent, CAMERA_REQUEST)
                } else {
                    ActivityCompat.requestPermissions(
                        this, listPermissionsNeeded.toTypedArray
                            (), REQUEST_ID_MULTIPLE_PERMISSIONS
                    )
                }
            }
            //delete img button
            delete_img.setOnClickListener()
            {
                imgLayout.visibility = View.GONE
                btnAddImage.visibility = View.VISIBLE
            }

            //client sign delete
            ivClientSignDel.setOnClickListener()
            {
                clientSignLayout.visibility = View.GONE
                btnClientSign.visibility = View.VISIBLE
            }

            ivSerPerSignDel.setOnClickListener()
            {
                serPerSignLayout.visibility = View.GONE
                btnSerPerSign.visibility = View.VISIBLE
            }


            //creating digital signature
            btnClientSign.setOnClickListener()
            {
                if (hasPermissions(listPermissionsNeeded)) {
                    mSignature = Signature(this, btnClientSign, ivClientSign,clientSignLayout)
                    mSignature.clear()
                    mSignature.dialogAction(this, dialog)
                } else {
                    ActivityCompat.requestPermissions(
                        this, listPermissionsNeeded.toTypedArray
                            (), REQUEST_ID_MULTIPLE_PERMISSIONS
                    )
                }

            }

            btnSerPerSign.setOnClickListener()
            {
                if (hasPermissions(listPermissionsNeeded)) {
                    mSignature = Signature(this, btnSerPerSign, ivSerPerSign,serPerSignLayout)
                    mSignature.clear()
                    mSignature.dialogAction(this, dialog)
                } else {
                    ActivityCompat.requestPermissions(
                        this, listPermissionsNeeded.toTypedArray
                            (), REQUEST_ID_MULTIPLE_PERMISSIONS
                    )
                }

            }


            //field Validations
            val listEditTextFilters: ArrayList<EdittextFilterModel> = ArrayList()
            listEditTextFilters.add(EdittextFilterModel(etServicePerName, Constants.REGEX.REGEX_ALPHABETS_FILEDS, 20, true, null))
            listEditTextFilters.add(EdittextFilterModel(etEmpId, Constants.REGEX.REGEX_ALPHANUMERIC_FILEDS, 20, true, null))
            listEditTextFilters.add(EdittextFilterModel(etCustName, Constants.REGEX.REGEX_ALPHABETS_FILEDS, 20, true, null))
            listEditTextFilters.add(EdittextFilterModel(etElevSerialNo, Constants.REGEX.REGEX_ALPHANUMERIC_FILEDS, 20, true, null))
            listEditTextFilters.add(EdittextFilterModel(etCustAddress, Constants.REGEX.REGEX_ADDRESS, 50, true, null))
           // listEditTextFilters.add(EdittextFilterModel(etCustEmail, "", 40, false, Patterns.EMAIL_ADDRESS))
            listEditTextFilters.add(EdittextFilterModel(etServiceDesc, Constants.REGEX.REGEX_ALPHANUMERIC_FILEDS, 40, true, null))

            MethodUtils.setEdittextValidation(listEditTextFilters)


            //initialize listeners
            btnGeneratePdf.setOnClickListener()
            {
                if (validateFormFields()) {
                    when (hasPermissions(listPermissionsNeeded)) {
                        true -> {
                            //tvDateTime.text = DateHelper.getDateTime(System.currentTimeMillis())
                            btnGeneratePdf.visibility = View.GONE
                            layoutBitmap = loadBitmapFromView(applicationLayout, applicationLayout.width, applicationLayout.height)
                            createPdf()
                        }
                        false -> {
                            ActivityCompat.requestPermissions(
                                this, listPermissionsNeeded.toTypedArray(),
                                REQUEST_ID_MULTIPLE_PERMISSIONS
                            )
                        }
                    }
                } else {
                    Handler().post {
                        try {
                            firstErrorView.performClick()
                            firstErrorView.requestFocus()
                            if (firstErrorView is EditText) {
                                MethodUtils.showKeyboard(this, firstErrorView)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    if (spServiceType.selectedItemPosition == 0)
                    {
                        Toast.makeText(this,"Please Select Servicing Type",Toast.LENGTH_SHORT).show()
                    }
                    else if(clientSignLayout.visibility==View.GONE||serPerSignLayout.visibility==View.GONE)
                    {
                        Toast.makeText(this,"Please Add Signature",Toast.LENGTH_SHORT).show()
                    }
                    else if(ivPhoto.visibility==View.GONE)
                    {
                        Toast.makeText(this,"Please Add Image",Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(this, "Please correct the invalid fields(s)", Toast.LENGTH_SHORT).show()
                    }
                }

            }

            //edit listener
            /* tvEditPdf.setOnClickListener()
             {
                applicationLayout.isEnabled = true;
                btnGeneratePdf.visibility = View.VISIBLE
             }*/

            btnViewPdf.setOnClickListener()
            {
                openGeneratedPDF()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val capturedImage: Bitmap = data.extras["data"] as Bitmap
                ivPhoto.setImageBitmap(capturedImage)
                btnAddImage.visibility = View.GONE
                imgLayout.visibility = View.VISIBLE
            }
        }
    }


    private fun createPdf() {

        val displayMetrics = DisplayMetrics()
        this.windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels.toFloat()
        val width = displayMetrics.widthPixels.toFloat()

        val convertHeight = height.toInt()
        val convertWidth = width.toInt()

        val document = PdfDocument()
        val pageInfo = PageInfo.Builder(convertWidth, convertHeight, 1).create()
        val page = document.startPage(pageInfo)

        val canvas = page.getCanvas()

        val paint = Paint()
        canvas.drawPaint(paint)

        layoutBitmap = Bitmap.createScaledBitmap(layoutBitmap, convertWidth, convertHeight, true)
        paint.color = Color.BLUE
        canvas.drawBitmap(layoutBitmap, 0.0f, 0.0f, null)
        document.finishPage(page)

        // write the document content
        val docsFolder = File(Environment.getExternalStorageDirectory().absolutePath + "/ElevatorApp/")
        if (!docsFolder.exists()) {
            docsFolder.mkdir()
        }
        pdfFilePath = File(docsFolder.absolutePath + "/" + getTimestamp() + ".pdf")

        if (!pdfFilePath.exists()) {
            Log.e("TAG", "Is file created " + pdfFilePath.createNewFile())
        }


        try {
            if (pdfFilePath.exists()) {
                document.writeTo(FileOutputStream(pdfFilePath))
                Log.e("TAG", "Write to File")
                document.close()
                Toast.makeText(this, "PDF is created!!!", Toast.LENGTH_SHORT).show()
                /*applicationLayout.isEnabled = false
                editModeLayout.visibility = View.VISIBLE
                btnGeneratePdf.visibility = View.GONE*/
            }

        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "Something wrong", Toast.LENGTH_LONG).show()
        }

        btnViewPdf.visibility = View.VISIBLE
        btnGeneratePdf.visibility = View.VISIBLE
        openGeneratedPDF()
    }


    private fun loadBitmapFromView(layoutView: View, width: Int, height: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        layoutView.draw(canvas)
        return bitmap
    }

    private fun getTimestamp(): String {
        val SAVE_FILE_FORMAT_FULL_DATE = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
        val currentTime = Date(System.currentTimeMillis())
        return "pdf_${SAVE_FILE_FORMAT_FULL_DATE.format(currentTime)}"

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                super.onOptionsItemSelected(item);
            }
        }
    }


    private fun openGeneratedPDF() {
        if (pdfFilePath.exists()) {
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = FileProvider.getUriForFile(
                this, BuildConfig.APPLICATION_ID +
                        ".fileprovider", pdfFilePath
            )
            //Uri.fromFile(pdfFilePath)
            intent.setDataAndType(uri, "application/pdf")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(this, "No Application available to view pdf", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun validationEdiText(et: EditText, ti: View): Int {
        if (ti is EditText) {
            if (TextUtils.isEmpty(et.text)) {
                ti.error = getString(R.string.mandatory_field)
                if (validCount == 0) {
                    firstErrorView = et
                }
                validCount++

            } else {
                ti.error = null
            }
        } else {
            if (TextUtils.isEmpty(et.text)) {
                (ti as TextInputLayout).error =  getString(R.string.mandatory_field)
                ti.isErrorEnabled = true
                if (validCount == 0) {
                    firstErrorView = et
                }
                validCount++
            } else {
                (ti as TextInputLayout).isErrorEnabled = false
                ti.error = null
            }
        }

        return validCount
    }

    private fun validateFormFields(): Boolean {
        validCount = 0

        validationEdiText(etServicePerName, etServicePerName)
        validationEdiText(etPdfMobleNo, etPdfMobleNo)
        validationEdiText(etEmpId, etEmpId)
        validationEdiText(etCustName, etCustName)
        validationEdiText(etCustAddress, etCustAddress)
        validationEdiText(etCustEmail,etCustEmail)
        validationEdiText(etElevSerialNo, etElevSerialNo)
        validationEdiText(etServicePerName, etServicePerName)

        if (spServiceType.selectedItemPosition == 0 || clientSignLayout.visibility==View.GONE||
            serPerSignLayout.visibility==View.GONE|| ivPhoto.visibility==View.GONE)
        {
            validCount++

        }
        return validCount == 0

    }


    //method to check permission list
    private fun hasPermissions(permissions: ArrayList<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && applicationContext != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(applicationContext, permission) !=
                    PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS && grantResults.isNotEmpty()) {
            for ((index, value) in grantResults.withIndex()) {
                if (value == PackageManager.PERMISSION_DENIED) {
                    when {
                        ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[index]) ->
                            ActivityCompat.requestPermissions(
                                this, listPermissionsNeeded.toTypedArray
                                    (), REQUEST_ID_MULTIPLE_PERMISSIONS
                            )
                        else ->
                            Snackbar.make(
                                this.findViewById(android.R.id.content),
                                R.string.grant_permision_msg,
                                Snackbar.LENGTH_LONG
                            ).setAction(R.string.enable) {
                                val intent = Intent()
                                intent.action = android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                intent.addCategory(Intent.CATEGORY_DEFAULT)
                                intent.data = Uri.parse("package:$packageName")
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                                startActivity(intent)
                            }.setActionTextColor(Color.WHITE).show()
                    }
                }
            }
        }
    }



    override fun onDestroy() {
        super.onDestroy()
    }
}
