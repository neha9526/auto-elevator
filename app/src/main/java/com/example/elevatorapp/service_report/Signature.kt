package com.example.elevatorapp.service_report

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.*
import android.net.Uri
import android.os.Environment
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageView
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.example.elevatorapp.R
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class Signature(context: Context,val btnSign:AppCompatButton,val ivSign:AppCompatImageView,val rlSign:RelativeLayout) : View(context) {

    // Creating Separate Directory for saving Generated Images
    var signDir = Environment.getExternalStorageDirectory().absolutePath + "/DigitSign/"

    private var signPath = ""

    private val paint = Paint()
    private val path = Path()

    private var lastTouchX: Float = 0.toFloat()
    private var lastTouchY: Float = 0.toFloat()
    private val dirtyRect = RectF()

    private val STROKE_WIDTH = 5f
    private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    private var bitmap:Bitmap? = null

    private var mClear: Button? = null
    private var mGetSign: Button? = null
    private var mCancel: Button? = null
    private var mContent: LinearLayout? = null
    var view: View? = null



    init {
        paint.isAntiAlias = true
        paint.color = Color.BLACK
        paint.style = Paint.Style.STROKE
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeWidth = STROKE_WIDTH
    }

    private fun save(v: View) {
        Log.e("tag", "Width: " + v.width)
        Log.e("tag", "Height: " + v.height)
        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(mContent!!.width, mContent!!.height, Bitmap.Config.RGB_565)

        }
        val canvas = Canvas(bitmap!!)
        try {
            if (!File(signDir).exists()) {
                File(signDir).mkdir()
            }

            val mFileOutStream = FileOutputStream(File(signPath))
            v.draw(canvas)
            // Convert the output file to Image such as .png
            bitmap!!.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream)
            mFileOutStream.flush()
            mFileOutStream.close()

        } catch (e: Exception) {
            Log.e("log_tag", e.toString())
        }
    }

     fun clear() {
        path.reset()
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        canvas.drawPath(path, paint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val eventX = event.x
        val eventY = event.y
        mGetSign!!.isEnabled = true

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                path.moveTo(eventX, eventY)
                lastTouchX = eventX
                lastTouchY = eventY
                return true
            }

            MotionEvent.ACTION_MOVE,

            MotionEvent.ACTION_UP -> {
                resetDirtyRect(eventX, eventY)
                val historySize = event.historySize
                for (i in 0 until historySize) {
                    val historicalX = event.getHistoricalX(i)
                    val historicalY = event.getHistoricalY(i)
                    expandDirtyRect(historicalX, historicalY)
                    path.lineTo(historicalX, historicalY)
                }
                path.lineTo(eventX, eventY)
            }
            else -> {
                debug("Ignored touch event: $event")
                return false
            }
        }

        invalidate(
            (dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
            (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
            (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
            (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt()
        )

        lastTouchX = eventX
        lastTouchY = eventY

        return true
    }

    private fun debug(string: String) {
        Log.v("log_tag", string)
    }

    private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY
        }
    }

    private fun resetDirtyRect(eventX: Float, eventY: Float) {
        dirtyRect.left = Math.min(lastTouchX, eventX)
        dirtyRect.right = Math.max(lastTouchX, eventX)
        dirtyRect.top = Math.min(lastTouchY, eventY)
        dirtyRect.bottom = Math.max(lastTouchY, eventY)
    }


    private fun getFileName(): String {
        val pic_name = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        return signDir + pic_name + ".png"
    }
    // Function for Digital Signature
    fun dialogAction(activity: Activity, dialog:Dialog):String {
        signPath = getFileName()
        mContent = dialog.findViewById(R.id.llDialog) as LinearLayout
        this.setBackgroundColor(Color.WHITE)
        // Dynamically generating Layout through java code
        mContent!!.addView(this, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = dialog.findViewById(R.id.clear) as Button
        mGetSign = dialog.findViewById(R.id.getsign) as Button
        mCancel = dialog.findViewById(R.id.cancel) as Button
        view = mContent

        mClear!!.setOnClickListener {
            Log.v("tag", "Panel Cleared")
            this.clear()
          //  mGetSign!!.isEnabled = false
        }

        mGetSign!!.setOnClickListener {
            Log.v("tag", "Panel Saved")
            view!!.isDrawingCacheEnabled = true
            this.save(view!!)
            dialog.dismiss()
            //Toast.makeText(context, "Successfully Saved", Toast.LENGTH_SHORT).show()
            btnSign.visibility = View.GONE
            rlSign.visibility = View.VISIBLE
            ivSign.setImageURI(Uri.fromFile(File(signPath)))
            this.clear()
            // Calling the same class
           //activity.recreate()


        }
        mCancel!!.setOnClickListener {
            Log.v("tag", "Panel Cancelled")
            dialog.dismiss()
            // Calling the same class
           // activity.recreate()
        }
        dialog.show()
        return signPath
    }

    class SignEvent(val signPath:String,val btnSign:AppCompatButton, val ivSign:AppCompatImageView)



}