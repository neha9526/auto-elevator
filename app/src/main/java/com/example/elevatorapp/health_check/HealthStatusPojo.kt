package com.example.elevatorapp.health_check

class HealthStatusPojo(val boardName:String,val healthStatus:Boolean,val subList:ArrayList<HealthStatusSubPojo>?) {

    class HealthStatusSubPojo(val boardName:String,val healthStatus:Boolean)
    {

    }
}