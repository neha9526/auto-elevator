package com.example.elevatorapp.health_check

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.example.elevatorapp.R
import com.example.elevatorapp.adapter.HealthCheckMainAdapter
import kotlinx.android.synthetic.main.activity_health_status.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class HealthStatusActivity : AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_status)

        //setting toolbar
        setSupportActionBar(customToolbar)
        activityName.text =  getString(R.string.health_check_menu)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //calling mainList fragment
        val newFragment = MainHealthListFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentHolder, newFragment).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            android.R.id.home -> {
              onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item);
            }
        }
    }

    override fun onBackPressed() {

        if(supportFragmentManager.backStackEntryCount>0)
        {
            supportFragmentManager.popBackStack()
        }
        else{
            super.onBackPressed()
        }
    }



}
