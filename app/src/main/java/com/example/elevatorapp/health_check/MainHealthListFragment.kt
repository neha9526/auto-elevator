package com.example.elevatorapp.health_check

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.elevatorapp.R
import com.example.elevatorapp.adapter.HealthCheckMainAdapter
import kotlinx.android.synthetic.main.layout_mainlist_fragment.*

class MainHealthListFragment : Fragment(),HealthCheckMainAdapter.OnItemClickListener{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.layout_mainlist_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setting reclerview layout manager
        rvMainList.layoutManager = LinearLayoutManager(activity)

        //adding data to main list
        val mainBoardList:ArrayList<HealthStatusPojo> = ArrayList()
        mainBoardList.add(HealthStatusPojo("Main Control Card",true,null))
        mainBoardList.add(HealthStatusPojo("Car Top Board",false,null))
        mainBoardList.add(HealthStatusPojo("Car Operating Panel (COP)",true,null))
        mainBoardList.add(HealthStatusPojo("Master Landing Board",false,null))
        mainBoardList.add(HealthStatusPojo("Landing Call Slaves",true,null))

        //set adapter
        rvMainList.adapter = HealthCheckMainAdapter(activity as Activity,mainBoardList,this)
    }


    override fun onItemClick() {
        val fragmentManager = activity!!.supportFragmentManager
        val newFragment = SubHealthListFragment()
        val transaction = fragmentManager.beginTransaction()
             transaction.addToBackStack(null)
            .replace(R.id.fragmentHolder, newFragment)
            .commit()
    }
}